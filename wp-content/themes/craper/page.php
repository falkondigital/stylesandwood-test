<?php get_header(); 
$meta = get_post_meta( get_the_id(), '_craper_page_meta', true); 
$sidebar = sh_set( $meta, 'page_sidebar' );
$layout = sh_set($meta, 'layout');?>

<article class="banner">
	<div class="container">
		<h2><?php the_title(); ?></h2>
		<div class="breadcrumbs">
			
			<ul class="breadcrumb">
				<?php echo get_the_breadcrumb(); ?>
			</ul>
		
		</div>
	</div>
</article>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
	<article role="main">
		<div class="section">
			<div class="container">
				<div class="row">
				
						<?php $class = ( $layout == 'full' ) ? 'col-xs-12 col-sm-8 col-md-12' : 'col-xs-12 col-sm-8 col-md-8';
							if( $sidebar && $layout == 'left' ): ?>
								<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
									<?php dynamic_sidebar( $sidebar ); ?>
								</div>
						<?php endif; ?>
				
						<div id="post-<?php the_ID(); ?>" <?php post_class( $class ); ?>>
							<div class="contents">
							
								<h2><?php the_title(); ?></h2>
								<div class="blog-box">
									<?php $no_image = ( !has_post_thumbnail() ) ? ' no-image' : ''; ?>
									
									<?php if( has_post_thumbnail() ): ?>
										<figure class="image image-hovered animated out<?php echo $no_image; ?>" data-animation="fadeInUp" data-delay="0"> <?php the_post_thumbnail('770x388');?>
											<figcaption>
												<h5><?php the_title();?></h5>
												<p><?php echo get_the_term_list(get_the_id(), 'category', '', ', '); ?></p>
											</figcaption>
										</figure>
									<?php endif; ?>
									
									
									<?php the_content();?>
									<?php wp_link_pages(array('before'=>'<div class="pagination">'.__('Pages: ', SH_NAME), 'after' => '</div>', 'link_before'=>'<span>', 'link_after'=>'</span>')); ?>
									

								</div>
								
								<?php if( have_comments() || comments_open() ) comments_template(); ?>
								
							</div>
						</div>
					
					<?php if( $sidebar && $layout == 'right' ): ?>
						<div class="col-xs-12 col-sm-4 col-md-4 sidebar">
							<?php dynamic_sidebar( $sidebar ); ?>
						</div>
					<?php endif; ?>
					
				</div>
			</div>
		</div>
	</article>

<?php endwhile; endif; ?>

<?php get_footer(); ?>
