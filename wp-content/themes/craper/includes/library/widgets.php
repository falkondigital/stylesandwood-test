<?php
//Store Categories
class SH_StoreCategories extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'StoreCategories', /* Name */__('Store Categories',SH_NAME), array( 'description' => __('This widget shows the list of store categories', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <?php $query = new WP_Query(array('showposts'=>sh_set($instance, 'number', 3), 'post_status'=>'publish', 'category__in'=>sh_set($instance, 'category'))); ?>
        <div class="col-md-7">
           	<?php echo $before_title.$title.$after_title; ?>

			<?php $terms = get_terms('product_cat', array('hide_empty'=>0));
			$array = $this->generate_terms($terms);?>

            <div class="row store-categories">
            
				<?php foreach( sh_set( $array, 'parent') as $p): ?>
                    
                    <div class="col-md-3 bullet_arrow2">
                        <h4><?php echo sh_set( $p, 'name'); ?></h4>
                        <?php if( $child = sh_set(sh_set( $array, 'child'), sh_set( $p, 'term_id')) ): ?>
                            <ul>
                                <?php foreach( (array)$child as $k ): ?>
                                    <li>
                                        <a href="<?php echo get_term_link($k, 'product_cat'); ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php printf(__('Total %s Items on this category', SH_NAME), sh_set( $k, 'count')); ?>"><?php echo sh_set($k, 'name'); ?></a>					
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
    
                <?php endforeach; ?>
			</div>            
		</div>
		<?php
		
		echo $after_widget;
	}
	
	
	function generate_terms( $terms )
	{
		$return = array();
		foreach( $terms as $t )
		{
			if( sh_set( $t, 'parent') ) $return['child'][sh_set( $t, 'parent')][sh_set( $t, 'term_id')] = $t;
			else $return['parent'][] = $t;
		}
		return $return;
	}
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Store Categories', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : 3;?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. Of Parent Categories:', SH_NAME);?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
        </p>
                
		<?php 
	}
}


//socialising
class SH_Socailizing extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Socailizing', /* Name */__('Socializing & Subscription',SH_NAME), array( 'description' => __('Subscribe to Feedburner Feed and show social icons', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <div class="col-md-5">
            <?php echo $before_title.$title.$after_title;?>

                <div class="row">
                    <p><?php echo sh_set($instance, 'text'); ?></p>
                    <div class="col-md-5 stay-connect">
                        <ul class="footer-social">
                            <?php $social = array('facebook'=>'Facebook', 'twitter'=>'Twitter', 'google-plus'=>'Google Plus', 'pinterest'=>'Pinterest','xing'=>'Xing','youtube'=>'Youtube','linkedin'=>'Linkedin','flickr'=>'Flickr','rss'=>'RSS Feed');
							foreach( $social as $s => $v):
								if( sh_set( $instance, $s ) ):?>
                            		<li><a href="<?php echo esc_url(sh_set( $instance, $s)); ?>" title="<?php echo esc_attr($v); ?>"><i class="icon-<?php echo $s; ?>"></i></a></li>
                                <?php endif;
                            endforeach; ?>
                            
                        </ul>
                    </div>
                
                <div class="col-md-6">
                    <div id="subscibe">
                        <h5><i class="icon-envelope"></i> <?php _e('Subscribe to Mail List', SH_NAME); ?></h5>
                        <form target="popupwindow" method="post" id="newletter_sub" action="http://feedburner.google.com/fb/a/mailverify">
			            
                    		<input type="text" class="textfield" id="email_address" name="email" placeholder="<?php _e('Email Address', SH_NAME); ?>" />
                            <input type="text" class="textfield" id="full_name" name="name" placeholder="<?php _e('Full Name', SH_NAME); ?>" />
                    		<input type="hidden" id="uri" name="uri" value="<?php echo esc_attr(sh_set($instance, 'feedburner')); ?>">
                    		<input type="hidden" value="en_US" name="loc">
                
                            <button class="btn btn-general" type="submit" name="S" ><?php _e('Subscribe', SH_NAME); ?></button>
                        
                        </form>

                    </div>
                </div>
           </div>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] = $new_instance['text'];
		$instance['feedburner'] = $new_instance['feedburner'];
		$social = array('facebook', 'twitter', 'google-plus', 'pinterest', 'xing','youtube','linkedin','flickr','rss');
		foreach( $social as $s ) $instance[$s] = $new_instance[$s];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Stay Connected', SH_NAME);
		$text = ($instance) ? esc_attr($instance['text']) : '';
		$feedburner = ( $instance ) ? esc_attr($instance['feedburner']) : '';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text:', SH_NAME);?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo $text; ?></textarea> 
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('feedburner'); ?>"><?php _e('Feedburner ID:', SH_NAME);?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('feedburner'); ?>" name="<?php echo $this->get_field_name('feedburner'); ?>" type="text" value="<?php echo esc_attr($feedburner); ?>" /> 
        </p>
        <p>
        	<h3><?php _e('Socializing', SH_NAME); ?></h3>
            <?php $social = array('facebook'=>'Facebook', 'twitter'=>'Twitter', 'google-plus'=>'Google Plus', 'pinterest'=>'Pinterest','xing'=>'Xing','youtube'=>'Youtube','linkedin'=>'Linkedin','flickr'=>'Flickr','rss'=>'RSS Feed'); ?>
            <?php foreach( $social as $k => $v ): ?>
                <label for="<?php echo $this->get_field_id($k); ?>"><?php echo $v;?></label>
                <input class="widefat" id="<?php echo $this->get_field_id($k); ?>" name="<?php echo $this->get_field_name($k); ?>" type="text" value="<?php echo esc_attr(sh_set($instance, $k)); ?>" /> 
        	<?php endforeach; ?>
        </p>
                
		<?php 
	}
}


//testimonials
class SH_Testimonials extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'TestimonialsSlider', /* Name */__('Testimonials',SH_NAME), array( 'description' => __('This widgtes is used to show clients testimonials slider', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;?>
        
        <div class="col-md-4">
        	<?php echo $before_title.$title.$after_title; ?>
            
			<div class="testimonials flexslider flex-direction-nav-on-top">
            
            	<?php $query = new WP_Query(array('post_type'=>'bistro_testimonial', 'showposts'=>sh_set($instance, 'number', 3), 'post_status'=>'publish')); ?>
				<ul class="slides">
					<?php while( $query->have_posts()): $query->the_post(); ?>
                    <li>
                    	<?php the_post_thumbnail('testimonial-thumb', array('class'=>'img-circle')); ?>
						<blockquote>
							<?php echo character_limiter( get_the_content(), sh_set($instance, 'limit', 100) ); ?>
							<small><?php the_title(); ?></small>
						</blockquote>
					</li>
                    <?php endwhile; ?>
					<?php wp_reset_query(); ?>
				</ul>
			</div>
		</div>
<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['limit'] = $new_instance['limit'];
		$instance['number'] = $new_instance['number'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Happy Testimonials', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : 3;
		$limit = ($instance) ? esc_attr($instance['limit']) : 50;?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. Of Testimonials:', SH_NAME);?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('limit'); ?>"><?php _e('Content Limit:', SH_NAME);?></label>
            <input id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="text" value="<?php echo esc_attr($limit); ?>" /> 
        </p>
        
        
	<?php 
	}
}


//Blog posts slider
class SH_BlogSlider extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'BlogSlider', /* Name */__('Bistro Recent Posts',SH_NAME), array( 'description' => __('This widget output the slider of blog posts with images', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		$column = sh_set( $instance, 'column' );
		echo $before_widget;?>
        <?php $query = new WP_Query(array('showposts'=>sh_set($instance, 'number', 3), 'post_status'=>'publish', 'category__in'=>sh_set($instance, 'category'))); ?>
        <div class="<?php echo ( $column == 1) ? 'col-md-3' : 'col-md-8'; ?>">
           	<?php echo $before_title.$title.$after_title; ?>

			<div class="recent-blog flexslider flex-direction-nav-on-top">
				<ul class="slides">
                	<?php $count = 1; ?>
					<?php while( $query->have_posts() ): $query->the_post(); ?>
                    	<?php if( $count == 1 ) echo '<li class="row">'; ?>
                        <div class="<?php echo ( $column == 1) ? 'col-md-12' : 'col-md-4'; ?> home-blog">
                            <div class="wdt-product">
                                <div class="wdt-products-wrapper">
                                    <div class="wdt-product active show">
                                        <?php if( has_post_thumbnail() ): ?>
                                        	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('post-slider'); ?></a>
                                        <?php endif; ?>
                                        <h3 class="post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                        <p class="post-meta"><?php echo get_the_date(); ?> // <?php comments_number();?></p>
                                        <div class="wdt-overlay zoom-hover"><span class="amount zoom"><i class="icon-camera"></i></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
						<?php if( $count == 3 || $column == 1 ) echo '</li>'; ?>
						<?php $count = ( $count == 3 || $column == 1 ) ? 1 : ( $count + 1 ); ?>
                  	<?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </ul>
			</div><!-- .recent-posts -->
		</div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['category'] = $new_instance['category'];
		$instance['column'] = $new_instance['column'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Recent Blog Posts', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : 3;
		$category = ($instance) ? esc_attr($instance['number']) : 0;
		$column = ( $instance ) ? esc_attr( $instance['column'] ) : 3;?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. Of Posts:', SH_NAME);?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', SH_NAME);?></label>
            <?php wp_dropdown_categories(array('show_option_all' => __('All Categories', SH_NAME), 'hide_empty'=>0, 'selected'=>$category, 'name'=>$this->get_field_name('category'), 'id'=>$this->get_field_id('category'))); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('column'); ?>"><?php _e('Column:', SH_NAME); ?></label>
            <select name="<?php echo $this->get_field_name('column'); ?>" id="<?php echo $this->get_field_id('column'); ?>">
            	<option value="1" <?php echo ( $column == 1 ) ? 'selected="selected"' : ''; ?>><?php _e('1 Column', SH_NAME ); ?></option>
                <option value="3" <?php echo ( $column == 3 ) ? 'selected="selected"' : ''; ?>><?php _e('3 Column', SH_NAME ); ?></option>
			</select>
        </p>
		<?php 
	}
}


/// Posts Tabber
class SH_Tabber extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Tabber', /* Name */__('Genuine Post Tabs ',SH_NAME), array( 'description' => __('Fetch the latest , Popular posts in tabber form', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$number_of_latest_posts = $instance['number_of_latest_posts'];
		$number_of_popular_posts = $instance['number_of_popular_posts'];
		
		echo $before_widget;?>
		
		<ul id="widgetTab" class="nav nav-tabs">
			<li class="active"><a href="#popular" data-toggle="tab"><?php echo $instance['label1']; ?></a></li>
			<li><a href="#recent" data-toggle="tab"><?php echo $instance['label2']; ?></a></li>
		</ul>
		
		<div id="widgetTabcontent" class="tab-content">
		   
			<div class="tab-pane fade in active" id="popular">
				<?php query_posts('orderby=comment_count&order=DESC&posts_per_page='.$number_of_popular_posts ) ; 
				$this->posts();
				wp_reset_query(); ?>
			</div>
			
			<div class="tab-pane fade" id="recent">
				<?php query_posts('posts_per_page='.$number_of_latest_posts);
				$this->posts();
				wp_reset_query(); ?>
				
			  </div>
			  
		</div>
		
		<?php echo $after_widget;
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['number_of_latest_posts'] = $new_instance['number_of_latest_posts'];
		$instance['number_of_popular_posts'] = $new_instance['number_of_popular_posts'];
		$instance['label1'] = $new_instance['label1'];
		$instance['label2'] = $new_instance['label2'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$number_of_latest_posts = ( $instance ) ? esc_attr($instance['number_of_latest_posts']) : 4;
		$number_of_popular_posts = ( $instance ) ? esc_attr($instance['number_of_popular_posts']) : 4;
		$label1 = ( $instance ) ? esc_attr($instance['label1']) : __('Popular', SH_NAME);
		$label2 = ( $instance ) ? esc_attr($instance['label2']) : __('Recent', SH_NAME);?>
			
        <p>
            <label for="<?php echo $this->get_field_id('label1'); ?>"><?php _e('Label for Popular Tab:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('label1'); ?>" name="<?php echo $this->get_field_name('label1'); ?>" type="text" value="<?php echo esc_attr($label1); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number_of_popular_posts'); ?>"><?php _e('No. of Popular Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_of_popular_posts'); ?>" name="<?php echo $this->get_field_name('number_of_popular_posts'); ?>" type="text" value="<?php echo esc_attr($number_of_popular_posts); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('label2'); ?>"><?php _e('Label for Recent Tab:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('label2'); ?>" name="<?php echo $this->get_field_name('label2'); ?>" type="text" value="<?php echo esc_attr($label2); ?>" />
        </p>
        
        <p>
            <label for="<?php echo $this->get_field_id('number_of_latest_posts'); ?>"><?php _e('No. of Latest Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number_of_latest_posts'); ?>" name="<?php echo $this->get_field_name('number_of_latest_posts'); ?>" type="text" value="<?php echo esc_attr($number_of_latest_posts); ?>" />
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>
        
            <ul class="recent_posts">
                
                <?php while( have_posts() ): the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('75x75'); ?>
                            <?php the_title(); ?>
                        </a>
                        <a href="<?php the_permalink(); ?>" class="readmore" title="<?php the_title(); ?>"><?php _e('read more', SH_NAME); ?></a>
                    </li>
                <?php endwhile; ?>
                
            </ul>
            
		<?php endif;
    }
}


/// Posts Tabber
class SH_Recent_Post extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Recent_Post', /* Name */__('Genuine Recent Posts ',SH_NAME), array( 'description' => __('Show the recent posts with images', SH_NAME )) );
	}
 

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;
		
		echo $before_title.$title.$after_title; 
		
		$query_string = 'posts_per_page='.$instance['number'];
		if( $instance['cat'] ) $query_string .= '&cat='.$instance['cat'];
		query_posts( $query_string ); 
	
		$this->posts();
		wp_reset_query(); 
		
		echo $after_widget;
	}
 
 
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		
		$instance['title'] = $new_instance['title'];
		$instance['number'] = $new_instance['number'];
		$instance['cat'] = $new_instance['cat'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ( $instance ) ? esc_attr($instance['title']) : __('Recent Posts', SH_NAME);
		$number = ( $instance ) ? esc_attr($instance['number']) : 4;
		$cat = ( $instance ) ? esc_attr($instance['cat']) : '';?>
			
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title: ', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. of Posts:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('cat'); ?>"><?php _e('Category', SH_NAME); ?></label>
            <?php wp_dropdown_categories( array('show_option_all'=>true, 'selected'=>$cat, 'class'=>'widefat') ); ?>
        </p>
        
		<?php 
	}
	
	function posts()
	{
		
		if( have_posts() ):?>
        
            <ul class="recent_posts">
                
                <?php while( have_posts() ): the_post(); ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
							<?php the_post_thumbnail('75x75'); ?>
                            <?php the_title(); ?>
                        </a>
                        <a href="<?php the_permalink(); ?>" class="readmore" title="<?php the_title(); ?>"><?php _e('read more', SH_NAME); ?></a>
                    </li>
                <?php endwhile; ?>
                
            </ul>
            
		<?php endif;
    }
}


// Subscribe to our mailing list
class SH_feedburner extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_subscribe_mail_list', /* Name */__('Genuine Subscribe to our Mailing List',SH_NAME), array( 'description' => __('create account on http://feedburner.com and allow users to subscribe', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        
        <?php echo $before_title . $title . $after_title ; ?>

        <form target="popupwindow" method="post" id="subscribe" action="http://feedburner.google.com/fb/a/mailverify" accept-charset="utf-8">
            <input class="form-control" type="text" name="name" value="" id="name" placeholder="<?php _e("Name" , SH_NAME); ?>">
            <input class="form-control" type="text" name="email" value="" id="email" placeholder="<?php _e("Your email address" , SH_NAME) ; ?>">
            <input type="hidden" id="uri" name="uri" value="<?php echo esc_attr($instance['ID']); ?>">
            <input type="hidden" value="en_US" name="loc">
            <div class="pull-right"><input class="button" type="submit" id="submit" name="submit" value="<?php _e("Subscribe to mail list &rarr; " , SH_NAME); ?>"></div>
            
        </form>

		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['ID'] = $new_instance['ID'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Subscribe to Our Mailing List', SH_NAME);
		$ID = ($instance) ? esc_attr($instance['ID']) : 'mixsms';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('ID'); ?>"><?php _e('Feedburner ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('ID'); ?>" name="<?php echo $this->get_field_name('ID'); ?>" type="text" value="<?php echo esc_attr($ID); ?>" />
        </p>
		<?php 
	}
}


// twitter
class SH_Twitter extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Twitter', /* Name */__('Bistro Tweets',SH_NAME), array( 'description' => __('Gryvh the latest tweets from twitter', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <div class="col-md-4">
            <?php echo $before_title.$title.$after_title; ?>
            <?php $number = (sh_set($instance, 'number') ) ? sh_set($instance, 'number') : 2; ?>
            <div class="sidebar-line"><span></span></div>
            <script type="text/javascript"> jQuery(document).ready(function($) {$('#twitter_update').sh_Tweets({screen_name: '<?php echo $instance['twitter_id']; ?>', number: <?php echo $number; ?>});});</script>
            <ul id="twitter_update"></ul>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['twitter_id'] = $new_instance['twitter_id'];
		$instance['number'] = $new_instance['number'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Recent Tweets', SH_NAME);
		$twitter_id = ($instance) ? esc_attr($instance['twitter_id']) : 'wordpress';
		$number = ( $instance ) ? esc_attr($instance['number']) : '';?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('twitter_id'); ?>"><?php _e('Twitter ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('twitter_id'); ?>" name="<?php echo $this->get_field_name('twitter_id'); ?>" type="text" value="<?php echo esc_attr($twitter_id); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Tweets:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        
                
		<?php 
	}
}


// Flicker Gallery
class SH_Flickr extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Flickr', /* Name */__('Bistro Flickr Feed',SH_NAME), array( 'description' => __('Fetch the latest feed from Flickr', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <div class="col-md-4">
            <?php echo $before_title.$title.$after_title; ?>
            <div class="sidebar-line"><span></span></div>
            <?php $limit = ($instance['number']) ? $instance['number'] : 8; ?>
            <script type="text/javascript">
				jQuery(document).ready(function($) {
					$('.flickr').jflickrfeed({
						limit: <?php echo $limit;?> ,
						qstrings: {id: '<?php echo $instance['flickr_id']; ?>'},
						itemTemplate: '<li><a href="{{link}}"><img src="{{image_s}}" alt="{{title}}" /></a></li>'
					});
				});
			</script>
            <ul class="flickr"></ul>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['flickr_id'] = $new_instance['flickr_id'];
		$instance['number'] = $new_instance['number'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Flickr Feed', SH_NAME);
		$flickr_id = ($instance) ? esc_attr($instance['flickr_id']) : '';
		$number = ( $instance ) ? esc_attr($instance['number']) : 8;?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('flickr_id'); ?>"><?php _e('Flickr ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('flickr_id'); ?>" name="<?php echo $this->get_field_name('flickr_id'); ?>" type="text" value="<?php echo esc_attr($flickr_id); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Tweets:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        
                
		<?php 
	}
}


// client slider
class SH_Clientslider extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Clientslider', /* Name */__('Bistro Clients Slider',SH_NAME), array( 'description' => __('Client Slider to show', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		$theme_options = _WSH()->option();
		$clients = sh_set( sh_set($theme_options, 'client_slider'), 'client_slider' );
		array_pop($clients);
		//printr($clients);
		if( isset( $clients['%s'] ) ) unset( $clients['%s'] );
		$chunk = array_chunk( $clients, 3 );//printr($chunk);
		echo $before_widget;?>
        <div class="col-md-6 clients-footer">
            <?php echo $before_title.$title.$after_title; ?>
            <div class="sidebar-line"><span></span></div>
            <div class="clients flexslider flex-direction-nav-on-top">
                <ul class="slides">
                    
                    	<?php foreach( $chunk as $cs ): ?>
                            <li>
                                <?php foreach( $cs as $c ):
                                    if( sh_set( $c, 'client_status' ) == 1 && sh_set( $c, 'logo' ) ): ?>
                                        
                                        <div class="col-md-4">                                
                                            <a href="<?php echo sh_set($c, 'client_link'); ?>"><img src="<?php echo sh_set( $c, 'logo' ); ?>" alt="" /></a>
                                        </div>
                                        
                                    <?php endif; 
                                endforeach;?>
                            </li>
                        <?php endforeach; ?>
                        
                    
                    
                </ul>
            </div>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];

		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Clients', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : '';?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of Slides:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" />
        </p>
        
                
		<?php 
	}
}

//Why us
class SH_WhyUs extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_WhyUs', /* Name */__('Bistro Why Us?',SH_NAME), array( 'description' => __('Show the Why us text', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <div class="col-md-4">
            <?php echo $before_title.$title.$after_title; ?>
            <div class="sidebar-line"><span></span></div>
            <p>
            	<?php echo $instance['text']; ?>
            </p>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] = $new_instance['text'];
	
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Why Us?', SH_NAME);
		$text = ($instance) ? esc_attr($instance['text']) : '';
		?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo $text; ?></textarea>
        </p>
        <?php 
	}
}

//Need Help
class SH_LiveChat extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_LiveChat', /* Name */__('Bistro Live Chat',SH_NAME), array( 'description' => __('Show the Need Help', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <div class="col-md-3">
            <?php echo $before_title.$title.$after_title; ?>
            <div class="sidebar-line"><span></span></div>
            <div class="livechat">
            	<span class="icon-comments"></span>
                	<h2><?php echo $instance['heading']; ?></h2>
					<p><?php echo $instance['text']; ?></p>
                    <img src="<?php $test=get_template_directory_uri(); echo $test; ?>/images/01_footer-cards.png" alt="" />
            </div>
        </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['heading'] = $new_instance['heading'];
		$instance['text'] = $new_instance['text'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Live Chat', SH_NAME);
		$heading = ($instance) ? esc_attr($instance['heading']) : '';
		$text = ( $instance ) ? esc_attr($instance['text']) : '';
		?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('heading'); ?>"><?php _e('Heading:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('heading'); ?>" name="<?php echo $this->get_field_name('heading'); ?>" type="text" value="<?php echo esc_attr($heading); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" ><?php echo $text; ?></textarea>
        </p>
        <?php 
	}
}

//New Items Widget
class SH_ProductSlider extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'ProductSlider', /* Name */__('Products Slider',SH_NAME), array( 'description' => __('This widget is flex slider to slide the products', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		echo $before_widget;?>

            <?php if ( $title )
                    echo $before_title . $title . $after_title;
                    $array = array();
                    if( $categories = sh_set( $instance, 'categories' ) ) $array['tax_query'] =  array( array( 'taxonomy' => 'product_cat', 'field' => 'id', 'terms' => $categories ) );
                    if( $counts = sh_set( $instance, 'count' ) ) $array['showposts'] = $counts;
                    $this->_args = $array; ?>
            
                <?php ///add_filter( 'bistro_slider_products_query_string', array( $this, 'filter_args') );
                        //get_template_part( 'includes/modules/products_slider' ); ?>
        <?php if( sh_set( $instance, 'slider' ) ): ?>
        	<div class="for-womans flexslider flex-direction-nav-on-top">
				<?php sh_include_file('includes/modules/products_slider_short.php', array('args'=>$array, 'ul_class'=>'slides', 'image_size'=>'product-slider1', 'lis'=>true ) ); ?>
            </div>
		<?php else:
			sh_include_file('includes/modules/products_slider_short.php', array('args'=>$array, 'ul_class'=>'thumbnails', 'span'=>'col-md-3', 'lis'=>true ) ); 
		endif; ?>
            
        <?php
		
		echo $after_widget;
	}
	
	function filter_args( $args )
	{
		return array_merge( $args, $this->_args );
	}

	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['count'] = $new_instance['count'];
		$instance['categories'] = $new_instance['categories'];
		$instance['slider'] = $new_instance['slider'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Latest Products', SH_NAME);
		$count = ($instance) ? esc_attr($instance['count']) : '8';
		$categories = ($instance) ? esc_attr($instance['categories']) : 0;
		$slider = ($instance) ? esc_attr( $instance['slider'] ) : 0; ?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                <?php _e('Title:', SH_NAME); ?>
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('count'); ?>">
                <?php _e('Number of Products:', SH_NAME); ?>
            </label>
            <input class="widefat" id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" type="text" value="<?php echo esc_attr($count); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('categories'); ?>">
                <?php _e('Category:', SH_NAME); ?>
            </label>
            <?php wp_dropdown_categories('show_option_all='.__('All', SH_NAME).'&hide_empty=0&name='.$this->get_field_name('categories').'&id='.$this->get_field_id('categories').'&selected='.$categories.'&taxonomy=product_cat'); ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('slider'); ?>"><?php _e('Slider?:', SH_NAME); ?></label>
            <select name="<?php echo $this->get_field_name('slider'); ?>" id="<?php echo $this->get_field_id('slider'); ?>">
            	<option value="1" <?php echo ( $slider == 1 ) ? 'selected="selected"' : ''; ?>><?php _e('True', SH_NAME ); ?></option>
                <option value="0" <?php echo ( $slider == 0 ) ? 'selected="selected"' : ''; ?>><?php _e('False', SH_NAME ); ?></option>
			</select>
        </p>
        <?php 
	}
}
/*--------------------------------------------------------------------------------------------------------*/

//Get In Touch
class SH_GetinTouch extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_GetinTouch', /* Name */__('Craper Get In Touch',SH_NAME), array( 'description' => __('Show the Contact information about the company', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        	<div class="widget widget-contact">
            	<?php echo $before_title.$title.$after_title; ?>
				<ul>
                	<li>Address: <?php echo $instance['address']; ?></li>
                    <li>Phone: <?php echo $instance['phone']; ?></li>
                    <li>Fax: <?php echo $instance['fax']; ?></li>
                    <li>Email: <a href="mailto:<?php echo $instance['email']; ?>"><?php echo $instance['email']; ?></a></li>
                </ul>
            </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['fax'] = $new_instance['fax'];
		$instance['email'] = $new_instance['email'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Get in Touch', SH_NAME);
		$address = ($instance) ? esc_attr($instance['address']) : '';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '';
		$fax = ( $instance ) ? esc_attr($instance['fax']) : '';
		$email = ( $instance ) ? esc_attr($instance['email']) : '';
		?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" ><?php echo $address; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('fax'); ?>"><?php _e('Fax:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('fax'); ?>" name="<?php echo $this->get_field_name('fax'); ?>" type="text" value="<?php echo esc_attr($fax); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>
                
		<?php 
	}
}

/*--------------------------------------------------------------------------------------------------------*/

//Find us here
class SH_FindUsHere extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_FindUsHere', /* Name */__('Craper Find Us Here',SH_NAME), array( 'description' => __('Show the Contact information about the company', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        	<div class="widget widget-contact">
            	<?php echo $before_title.$title.$after_title; ?>
				<ul class="unstyled">
                	<li><strong><?php echo $instance['country']; ?></strong></li>
					<li><strong>Address: </strong> <?php echo $instance['address']; ?></li>
                    <li><strong>Phone: </strong> <?php echo $instance['phone']; ?></li>
                    <li><strong>Fax: </strong> <?php echo $instance['fax']; ?></li>
                    <li><strong>Email: </strong> <a href="mailto:<?php echo $instance['email']; ?>"><?php echo $instance['email']; ?></a></li>
                	<li><strong>Website: </strong> <?php echo $instance['website']; ?></li>
				</ul>
            </div>
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['country'] = $new_instance['country'];
		$instance['address'] = $new_instance['address'];
		$instance['phone'] = $new_instance['phone'];
		$instance['fax'] = $new_instance['fax'];
		$instance['email'] = $new_instance['email'];
		$instance['website'] = $new_instance['website'];
		
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Find us here', SH_NAME);
		$country = ($instance) ? esc_attr($instance['country']) : '';
		$address = ($instance) ? esc_attr($instance['address']) : '';
		$phone = ( $instance ) ? esc_attr($instance['phone']) : '';
		$fax = ( $instance ) ? esc_attr($instance['fax']) : '';
		$email = ( $instance ) ? esc_attr($instance['email']) : '';
		$website = ( $instance ) ? esc_attr($instance['website']) : '';
		?>
        
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('country'); ?>"><?php _e('Country:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('country'); ?>" name="<?php echo $this->get_field_name('country'); ?>" type="text" value="<?php echo esc_attr($country); ?>" />
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', SH_NAME); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" ><?php echo $address; ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo esc_attr($phone); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('fax'); ?>"><?php _e('Fax:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('fax'); ?>" name="<?php echo $this->get_field_name('fax'); ?>" type="text" value="<?php echo esc_attr($fax); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email ID:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($email); ?>" />
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('website'); ?>"><?php _e('Website:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('website'); ?>" name="<?php echo $this->get_field_name('website'); ?>" type="text" value="<?php echo esc_attr($website); ?>" />
        </p>
                
		<?php 
	}
}


//Recent Posts
class SH_RecentPosts extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_RecentPosts', /* Name */__('Craper Recent Posts',SH_NAME), array( 'description' => __('This widget output the Recent posts', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <?php $query = new WP_Query(array('showposts'=>sh_set($instance, 'number', 5), 'post_status'=>'publish', 'category__in'=>sh_set($instance, 'category'))); ?>
        	<div class="widget widget_recent_entries">
           	
			<?php echo $before_title.$title.$after_title; ?>
		
				<ul class="bullet-2">
                	<?php while( $query->have_posts() ): $query->the_post(); ?>
                    	 <li><a href="<?php the_permalink();?>"><?php the_title();?></a></li>	
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </ul>
			</div><!-- .recent-entries -->
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['category'] = $new_instance['category'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Recent Posts', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : 5;
		$category = ($instance) ? esc_attr($instance['number']) : 0;
		?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. Of Posts:', SH_NAME);?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', SH_NAME);?></label>
            <?php wp_dropdown_categories(array('show_option_all' => __('All Categories', SH_NAME), 'hide_empty'=>0, 'selected'=>$category, 'name'=>$this->get_field_name('category'), 'id'=>$this->get_field_id('category'))); ?>
        </p>
        
		<?php 
	}
}

//Portfolio Gallery
class SH_Gallery extends WP_Widget
{
	/** constructor */
	function __construct()
	{
		parent::__construct( /* Base ID */'SH_Gallery', /* Name */__('Craper Portfolio Gallery',SH_NAME), array( 'description' => __('This widget output the Recent portfolios', SH_NAME )) );
	}

	/** @see WP_Widget::widget */
	function widget($args, $instance)
	{
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );
		
		echo $before_widget;?>
        <?php $query = new WP_Query(array('post_type'=>'sh_portfolio','showposts'=>sh_set($instance, 'number', 6), 'post_status'=>'publish', 'category__in'=>sh_set($instance, 'category'))); ?>
        	<div class="widget widget-gallery">
           	
			<?php echo $before_title.$title.$after_title; ?>
		
				<ul>
                	<?php while( $query->have_posts() ): $query->the_post(); ?>
                    	 <?php
							$url = wp_get_attachment_url( get_post_thumbnail_id(get_the_id()) );
							//$url = $thumb['0'];
						 ?>
						 <li><a href="<?php echo $url;?>" data-rel="prettyPhoto" title="<?php the_title();?>"><?php the_post_thumbnail('80x80');?></a></li>	
                    <?php endwhile; ?>
                    <?php wp_reset_query(); ?>
                </ul>
			</div><!-- .recent-entries -->
		<?php
		
		echo $after_widget;
	}
	
	
	/** @see WP_Widget::update */
	function update($new_instance, $old_instance)
	{
		$instance = $old_instance;

		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = $new_instance['number'];
		$instance['category'] = $new_instance['category'];
		return $instance;
	}

	/** @see WP_Widget::form */
	function form($instance)
	{
		$title = ($instance) ? esc_attr($instance['title']) : __('Gallery Widget', SH_NAME);
		$number = ($instance) ? esc_attr($instance['number']) : 6;
		$category = ($instance) ? esc_attr($instance['category']) : 0;
		?>
        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SH_NAME); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('No. Of Posts:', SH_NAME);?></label>
            <input id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="text" value="<?php echo esc_attr($number); ?>" /> 
        </p>
        <p>    
            <label for="<?php echo $this->get_field_id('category'); ?>"><?php _e('Category:', SH_NAME);?></label>
            <?php wp_dropdown_categories(array( 'taxonomy' => 'portfolio_category', 'show_option_all' => __('All Categories', SH_NAME), 'hide_empty'=>0, 'selected'=>$category, 'name'=>$this->get_field_name('category'), 'id'=>$this->get_field_id('category'))); ?>
        </p>
        
		<?php 
	}
}










