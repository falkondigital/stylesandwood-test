<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<div class="col-xs-12">
	<h4><?php echo $title; ?></h4>
	<ul class="bullet-1">
		<?php $list = explode("\n",$text);
			foreach ($list as $value) {
		?>	

		<li class="animated out" data-animation="fadeInLeft" data-delay="0"><?php echo $value;?></li>

		<?php } ?>
	</ul>
</div>

<?php  
$output = ob_get_contents();
ob_end_clean();

?>