	<h4><?php _e('Project Description', SH_NAME); ?></h4>
	<p><?php the_excerpt();?></p>
	<h4><?php _e('Features', SH_NAME); ?></h4>
	<ul class="bullet-2">
	
		<?php $features = sh_set($meta, 'feature_group');

		$delay = 0;
		
		foreach($features as $key => $value):?>
					
			<li class="animated out" data-animation="fadeInLeft" data-delay="<?php echo $delay;?>"><?php echo sh_set( $value, 'feature' ); ?></li>
		
			<?php $delay += 200; 
		endforeach; ?>
		
	</ul>
	<h4><?php _e('Client', SH_NAME); ?></h4>
	<p><?php echo sh_set($meta, 'client_name');?> <br>
		<?php _e('website ', SH_NAME); ?><a href="<?php echo sh_set($meta, 'project_url');?>"><?php echo sh_set($meta, 'project_url');?></a></p>
