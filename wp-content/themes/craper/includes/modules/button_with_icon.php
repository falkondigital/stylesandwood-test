<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php if($btn_icon == 'left'){?>

	<a href="<?php echo $btn_link;?>" class="btn <?php echo $btn_style;?> <?php echo $btn_size;?> btn-icon-hold"> <i class="icon-page-arrow-right "></i><span class="btn-text"><?php echo $btn_text;?></span> </a>

<?php }else{?>

	<a href="<?php echo $btn_link;?>" class="btn <?php echo $btn_style;?> <?php echo $btn_size;?> btn-icon-hold"> <span class="btn-text"><?php echo $btn_text;?></span><i class="icon-page-arrow-right "></i> </a>

<?php }?>	

<?php  
$output = ob_get_contents();
ob_end_clean();

?>