<?php $t = $GLOBALS['_sh_base'];
ob_start();
?>

<?php if( have_posts()):?>
<div class="col-xs-12 animated out" data-animation="fadeInLeft" data-delay="100">
	<div class="testimonial caro-1-col">
		<?php if($pagination == 'top'):?><div class="caro-pagination"></div><?php endif;?>
		<ul class="caro-slider-ul">
		
			<?php while( have_posts()): the_post();
							$meta = get_post_meta( get_the_id(), '_sh_testimonial_meta', true );//printr($meta);?>
			<li>
				<div class="testim">
					<p><?php echo $this->excerpt(get_the_excerpt(), 300); ?></p>
					<div class="thumblist">
						<ul>
							
							<!-- Thumbnail List Item Start -->
							<li>
								<figure> <?php the_post_thumbnail('100x100');?> </figure>
								<div class="text">
									<h4><?php the_title();?></h4>
									<p class="theme-color"><?php echo sh_set($meta, 'designation');?></p>
									<p><?php echo sh_set($meta, 'company');?></p>
								</div>
							</li>
						</ul>
					</div>
					<!-- /thumblist --> 
					
				</div>
			</li>
			<?php endwhile; ?>
			
		</ul>
		<?php if($pagination == 'bottom'):?><div class="caro-pagination"></div><?php endif;?>
	</div>
</div>
<?php endif; 

$output = ob_get_contents();
ob_end_clean();

?>