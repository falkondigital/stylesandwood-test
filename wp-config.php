<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'swtest');

/** MySQL database username */
define('DB_USER', 'wordpress_admin');

/** MySQL database password */
define('DB_PASSWORD', 'CXv7AFsCzKhbFTY6');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '<cPO-J`9}s%`94i&EI,ZzI4%r( =/K}Tk#BlXM)j-%>IK(0a]}CGruFkSCnN!Dn_');
define('SECURE_AUTH_KEY',  'jOXI5BJJp<W&3tv3(k-4PI$A@nWkIMMLz!bwW L$qglyk<wgLkY46LS? ]QQ-9;f');
define('LOGGED_IN_KEY',    'M<b@K855Tv{JG_5v`,Q/!1GsuImCUUmK59Z2z<6*|T5U_8g[!qs[Q$rngFIUeiLd');
define('NONCE_KEY',        'iLY.{Q.*b^XHhMCzn63#qiN]:2FO<WXVGaELhYZVcIt5vz<a,ZX#KHZ|Pz:NW.Kr');
define('AUTH_SALT',        '9`x#kv;<lMU(],}Xa_4TX]NG#kr~2x{[uW/)SqhfhD;HbGj:_a[x.q- +(CavxZp');
define('SECURE_AUTH_SALT', '/71cYQjA0&Jg{gPj4bV&)$*U!4z}3<cV<4(mUz=y3kgT_;IV69oHBw+SaD BGZ^y');
define('LOGGED_IN_SALT',   'H7UpS*mKw7s`;/Vt>$S!(Wb,?K-IPG4QD$bE,WuBUiqkz:q]G=2-t|@A+eyrvPgp');
define('NONCE_SALT',       '70pTz4pw1nS+jxAvpxux@y``AmC~Jf2(, 8WD|SU;+5@i24ac~#6+w-(sLx.C6{V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
